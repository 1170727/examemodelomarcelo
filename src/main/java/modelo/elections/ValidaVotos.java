/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.elections;

/**
 * interface com métodos que validam votos
 * 
 * @author Marcelo
 */
public interface ValidaVotos {
    public abstract boolean verificaData();
}
