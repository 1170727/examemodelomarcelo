/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.elections;

/**
 * Ponto 3 do enunciado do exame modelo
 * Modela um voto eletrónico (herda de voto e implementa ValidaVotos)
 * @author Marcelo
 */
public class VotoEletronico extends Voto implements ValidaVotos{
    /**
     * Instancia a classe quando o candidato, a data e a dataLimite são inseridos como parâmetro
     * @param candidato
     * @param data
     * @param dataLimite 
     */
    public VotoEletronico(Candidato candidato, int data, int dataLimite){
        super(candidato, data,dataLimite);
    }
    /**
     * Valida a o voto eletrónico
     * Porpositadamente ambíguo, de modo a possibilitar posterior alteração do paradigma de validação
     * @return verificaData()
     */
    @Override
    public boolean eValido(){
        return verificaData();
    }
    /**
     * Valida a data do envio do voto eletrónico
     * @return true if data<dataLimite-2; else false
     */
    @Override
    public boolean verificaData(){
        return getData() < getDataLimite()-2;
    }
    /**
     * Retorna uma descrição textual do objeto
     * "envio do voto eltrónico para (nome do candidato aqui) -> válido/inválido"
     * @return descrição textual
     */
    @Override
    public String toString(){
        return String.format("envio voto elrtrónico para %s", super.toString());
    }
}
