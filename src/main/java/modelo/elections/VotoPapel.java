/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.elections;

/**
 *
 * @author vibog
 */
public class VotoPapel extends Voto {
    private boolean assinado;
    
    
    public VotoPapel (Candidato candidato, int data, int dataLimite, boolean assinado){
        super(candidato, data, dataLimite);
        this.assinado = assinado;
    }
    @Override
    public boolean eValido(){
        return assinado;
    }
    @Override
    public String toString(){
       return String.format("envio de voto em papel para %s",super.toString()); 
    }
    
}
