/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.elections;

/**
 * Ponto 1 do exame modelo 
 * Modela o candidato numa eleição partidária
 *
 * @author Marcelo
 */
public class Candidato {

    /**
     * Nome do candidato
     */
    private String nome;
    /**
     * Numero de votos feitos no candidato
     */
    private int numeroVotos;

    /**
     * Instancia a classe quando uma String é inserida como parâmetro O atributo
     * numeroVotos é inicializado a 0
     *
     * @param nome
     */
    public Candidato(String nome) {
        this.nome = nome;
        this.numeroVotos = 0;

    }

    /**
     * Incrementa o atributo numeroVotos por um
     */
    public void incrementarVotos() {
        this.numeroVotos++;
    }

    /**
     * Muda o valor do atributo numeroVotos para 0;
     */
    public void inicializarNumVotos() {
        this.numeroVotos = 0;
    }

    /**
     * Retorna o numero de votos
     *
     * @return numeroVotos
     */
    public int getNumVotos() {
        return this.numeroVotos;
    }

    /**
     * Retorna o nome
     *
     * @return nome
     */
    public String getNome() {
        return this.nome;
    }
}
