/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.elections;

/**
 * Ponto 3 do enunciado do exame modelo Modela um voto para um candidato numa
 * eleição
 *
 * @author Marcelo
 */
public class Voto {

    /**
     * Candaidato no qual o votante votou
     */
    private Candidato candidato;
    /**
     * Data em que o voto foi efetuado
     */
    private int data;
    /**
     * data limite apartir da qual o voto é invalido
     */
    private int dataLimite;

    /**
     * Instancia a classe quando um candidato, uma data e uma dataLimite são
     * introduzidos como parâmetro
     *
     * @param candidato
     * @param data
     * @param dataLimite
     */
    public Voto(Candidato candidato, int data, int dataLimite) {
        this.candidato = candidato;
        this.data = data;
        this.dataLimite = dataLimite;
    }

    /**
     * Valida o voto consoante a data em que foi feito e a sua data limite
     *
     * @return true if data<dataLimite, false if data> dataLimite
     */
    public boolean eValido() {
        return data < dataLimite;
    }

    /**
     * Retorna uma descrição textual do voto
     *
     * @return descriçãoTextual
     */
    @Override
    public String toString() {
        String estado;
        if(eValido()){
            estado = "Válido";
            
        } else {
            estado = "Inválido";
        }
        return String.format("%s -> %s", candidato.getNome(), estado);
    }

    /**
     * Retora o candidato associado ao voto
     *
     * @return canddato
     */
    public Candidato getCandidato() {
        return this.candidato;
    }

    /**
     * Retorna a data em que o voto foi feito
     *
     * @return data
     */
    public int getData() {
        return this.data;
    }

    /**
     * Retorna a data limite apartir da qual o voto é inválido
     *
     * @return dataLimit
     */
    public int getDataLimite() {
        return this.dataLimite;
    }

}
