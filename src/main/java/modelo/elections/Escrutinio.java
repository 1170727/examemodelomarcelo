/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.elections;
import java.util.ArrayList;
/**
 * 
 * Ponto 2 do exame modelo
 * Modela o processo de averiguação do candidato vencedor de uma eleição partidária (comunas)
 * 
 * @author Marcelo
 */
public class Escrutinio {
    /**
     * Lista de todos os candidatos a vencer a eleição
     */
    private ArrayList <Candidato> candidatos;
    /**
     * Número mázimo de eleitores (militantes do partido)
     */
    private int numEleitores;
    /**
     * Número de pessoas que efetivamente votaram
     */
    private int numVot;
    /**
     * dia da eleição (inteiro para smplificar
     */
    private int data;
    /**
     * Instancia a classe quando os candidatos, o numero de eleitores e a data são introduzidos como parâmetro. O número de votantes efetivos é inicializado a 0
     * @param candidatos
     * @param numEleitores
     * @param data 
     */
    public Escrutinio(ArrayList <Candidato> candidatos, int numEleitores, int data){
        this.candidatos = candidatos;
        this.numEleitores = numEleitores;
        this.numVot = 0;
        this.data = data;
    }
    /**
     * Soma o número de votos que cada candidato teve, de modo a determinar onúmero de pessoas que votaram, atribuindo esse valor ao atriubot numVotos
     */
    public void calcularVotantes(){
        for (Candidato candidato : candidatos) {
            this.numVot += candidato.getNumVotos();
        }
    }
    /**
     * Muda o valor do número de votos em cada candidato para 0
     */
    public void inicializarNumVotos(){
        for (Candidato candidato : candidatos) {
            candidato.inicializarNumVotos();
        }
    }
    
    /**
     * Seleciona o(s) candidato(s) com mais votos
     * @return um arrayList contendo o(s) candidato(s) mais votado(s)
     */
    public ArrayList<Candidato> vencedor(){
        ArrayList <Candidato> vencedores = new ArrayList();
        int numMaiorVotos = 0;
        for (Candidato candidato : candidatos) {
            if(candidato.getNumVotos() > numMaiorVotos){
                numMaiorVotos = candidato.getNumVotos();
            }
            
        }
        for (Candidato candidato : candidatos) {
            if(candidato.getNumVotos() == numMaiorVotos){
                vencedores.add(candidato);
            }
        }
        
        return vencedores;
    }
}
